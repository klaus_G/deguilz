package Datos;

import ListasEncadenadas.*;
import Logica.Generador;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.Iterator;

public class Pasajero {

    private String nombreCompleto;
    private String correoElectronico;
    private String contrasena;
    private String fotoPerfil;
    private Chain<Vehiculo> vehiculos;
    private Chain<Ruta> rutas;
    private Chain<Viaje> viajes;
    private String fotoLicencia;
    private boolean conductor;

    public Pasajero(String nombreCompleto, String correoElectronico, String contrasena, String fotoPerfil, Chain<Vehiculo> vehiculos, Chain<Ruta> rutas, Chain<Viaje> viajes, String fotoLicencia, boolean conductor) {
        this.nombreCompleto = nombreCompleto;
        this.correoElectronico = correoElectronico;
        this.contrasena = contrasena;
        this.fotoPerfil = fotoPerfil;
        this.vehiculos = vehiculos;
        this.rutas = rutas;
        this.viajes = viajes;
        this.fotoLicencia = fotoLicencia;
        this.conductor = conductor;
    }

    public Pasajero(String nombreCompleto, String correoElectronico, String contrasena, String fotoPerfil) {
        this.nombreCompleto = nombreCompleto;
        this.correoElectronico = correoElectronico;
        this.contrasena = contrasena;
        this.fotoPerfil = fotoPerfil;
    }

    public static void registrar(Pasajero pasajero) {
        String[] s = Pasajero.leer();
        String ss = "";
        if (s != null) {
            for (String p : s) {
                ss = ss + p + "/";
            }
        }
        FileWriter writer = null;

        try {
            writer = new FileWriter("Pasajeros.txt");
            writer.write(ss + pasajero.plot() + "/");
            writer.close();
        } catch (Exception ex) {

        }

    }

    public static void mostrar() {
        Chain<Pasajero> p = pasarAlista();
        for (int i = 0; i < p.size(); i++) {
            System.out.println(p.get(i).plot());
        }
    }

    public void agregarVehiculo(Vehiculo vehiculo) {

        if (this.vehiculos == null) {
            Chain<Vehiculo> vehiculos = new Chain<>();
            this.vehiculos = vehiculos;
        }

        this.vehiculos.add(vehiculo);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void eliminarVehiculo(Vehiculo vehiculo) {

        if (this.vehiculos == null) {
            Chain<Vehiculo> vehiculos = new Chain<>();
            this.vehiculos = vehiculos;
        }

        this.vehiculos.add(vehiculo);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";

        for (int i = 0; i < this.vehiculos.size(); i++) {
            if (this.vehiculos.get(i).getPlaca().equals(vehiculo.getPlaca())) {
                this.vehiculos.remove(i);
            }
        }

        for (int i = 0; i < chain.size(); i++) {
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void agregarRuta(Ruta ruta) {

        if (this.rutas == null) {
            Chain<Ruta> rutas = new Chain<>();
            this.rutas = rutas;
        }

        this.rutas.add(ruta);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);

    }

    public void eliminarRuta(Ruta ruta) {

        if (this.rutas == null) {
            Chain<Ruta> rutas = new Chain<>();
            this.rutas = rutas;
        }

        this.rutas.add(ruta);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < this.rutas.size(); i++) {
            if (this.rutas.get(i).getCodigo().equals(ruta.getCodigo())) {
                this.rutas.remove(i);
            }
        }

        for (int i = 0; i < chain.size(); i++) {
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public static Chain<Viaje> todosViajes() {
        Chain<Pasajero> chain = pasarAlista();
        Chain<Viaje> viajes = new Chain<>();

        for (int i = 0; i < chain.size(); i++) {
            for (int j = 0; j < chain.get(i).getViajes().size(); j++) {

                viajes.add(chain.get(i).getViajes().get(j));

            }
        }

        return viajes;
    }

    public void agregarViaje(Viaje viaje) {

        if (this.viajes == null) {
            Chain<Viaje> viajes = new Chain<>();
            this.viajes = viajes;
        }

        this.viajes.add(viaje);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);

    }

    public void eliminarViaje(Viaje viaje) {

        if (this.viajes == null) {
            Chain<Viaje> viajes = new Chain<>();
            this.viajes = viajes;
        }

        this.viajes.add(viaje);
        Chain<Pasajero> chain = pasarAlista();
        String s = "";

        for (int i = 0; i < this.viajes.size(); i++) {
            if (this.viajes.get(i).getCodRuta().equals(viaje.getCodRuta())) {
                this.viajes.remove(i);
            }
        }

        for (int i = 0; i < chain.size(); i++) {
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public static Pasajero consultar(String email) {
        Chain<Pasajero> chain = Pasajero.pasarAlista();
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).correoElectronico.equals(email)) {
                return chain.get(i);
            }
        }
        return null;
    }

    public static boolean iniciarSesion(String email, String contrasena) {
        boolean a = false;
        Chain<Pasajero> chain = Pasajero.pasarAlista();

        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(email) && chain.get(i).getContrasena().equals(contrasena)) {
                a = true;
            }
        }
        return a;
    }

    private void actualizar(String s) {
        FileWriter writer = null;

        try {
            writer = new FileWriter("Pasajeros.txt");
            writer.write(s);
            writer.close();
        } catch (Exception ex) {

        }
    }

    public static Chain<Pasajero> pasarAlista() {
        String[] objetos = Pasajero.leer();
        Chain<Vehiculo> vehiculos = null;
        Chain<Ruta> rutas = null;
        Chain<Viaje> viajes = null;
        String fotoLicencia = null;
        boolean conductor;
        Chain<Pasajero> chain = new Chain<>();
        if (objetos != null) {
            for (String objeto : objetos) {
                String[] un = objeto.split(",");
                conductor = un[8].equals("true") ? true : false;
                if (!un[4].equals("null")) {
                    vehiculos = new Chain<>();
                    String[] vehiculosArray = un[4].split("~");

                    for (String string : vehiculosArray) {
                        String[] atributos = string.split("#");
                        Vehiculo vehiculo = new Vehiculo(atributos[0], atributos[1], atributos[2], atributos[3], Integer.parseInt(atributos[4]));
                        vehiculos.add(vehiculo);
                    }

                }

                if (!un[5].equals("null")) {
                    rutas = new Chain<>();
                    String[] rutasArray = un[5].split("$");
                    
                    for (String string : rutasArray) {
                        String[] atributos = string.split("%");
                        Generador generador = new Generador();
                        String codigo = String.valueOf(generador.randomNum(0, 1000000000)) + generador.generarCadenaSinSimbolosLongitudFija(54, 96, 5);
                        
                        Ruta ruta = new Ruta(atributos[0], atributos[1], atributos[2], codigo);
                        rutas.add(ruta);
                    }

                }

                if (!un[6].equals("null")) {
                    viajes = new Chain<>();
                    String[] viajesArray = un[6].split("&");

                    for (String string : viajesArray) {
                        String[] atributos = string.split("=");
                        Viaje viaje = new Viaje(atributos[0], atributos[1], atributos[2], atributos[3], atributos[4], atributos[5]);
                        viajes.add(viaje);
                    }

                }

                Pasajero pasajero = new Pasajero(un[0], un[1], un[2], un[3], vehiculos, rutas, viajes, un[7], conductor);
                chain.add(pasajero);
            }
        }
        return chain;
    }

    public static String[] leer() {

        try {
            FileReader fileReader = new FileReader("Pasajeros.txt");
            BufferedReader reader = new BufferedReader(fileReader);
            String s = reader.readLine();
            String[] a = null;
            a = (s != null) ? s.split("/") : null;
            return a;
        } catch (Exception ex) {
             throw new Error(":(");
        }

    }

    public void eliminar() {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void updateNombreCompleto(String nombreCompleto) {

        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setNombreCompleto(nombreCompleto);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void updateCorreoElectronico(String correoElectronico) {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setCorreoElectronico(correoElectronico);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void updateContrasena(String contrasena) {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setContrasena(contrasena);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getFotoPerfil() {
        return fotoPerfil;
    }

    public void updateFotoPerfil(String fotoPerfil) {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setFotoPerfil(fotoPerfil);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public Chain<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Chain<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public Chain<Ruta> getRutas() {
        return rutas;
    }

    public void setRutas(Chain<Ruta> rutas) {
        this.rutas = rutas;
    }

    public Chain<Viaje> getViajes() {
        return viajes;
    }

    public void setViajes(Chain<Viaje> viajes) {
        this.viajes = viajes;
    }

    public String getFotoLicencia() {
        return fotoLicencia;
    }

    public void updateFotoLicencia(String fotoLicencia) {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setFotoLicencia(fotoLicencia);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);
    }

    public void setFotoLicencia(String fotoLicencia) {

    }

    public boolean isConductor() {
        return conductor;
    }

    public void updateConductor(boolean conductor) {
        Chain<Pasajero> chain = pasarAlista();
        String s = "";
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getCorreoElectronico().equals(this.correoElectronico)) {
                chain.remove(i);
                this.setConductor(conductor);
                chain.add(this);
            }
            s += chain.get(i).plot() + "/";
        }

        actualizar(s);

    }

    public void setConductor(boolean conductor) {
        this.conductor = conductor;
    }

    public String plot() {
        String s = nombreCompleto + "," + correoElectronico + "," + contrasena + "," + fotoPerfil + ",";

        if (vehiculos != null) {
            s += "";
            for (int i = 0; i < this.vehiculos.size(); i++) {
                s += vehiculos.get(i).plot();
                if (i < this.vehiculos.size()) {
                    s += "~";
                }
            }
            s += ",";
        } else {
            s += "null,";
        }

        if (rutas != null) {
            s += "";
            for (int i = 0; i < this.rutas.size(); i++) {
                s += rutas.get(i).plot();
                if (i < this.rutas.size()) {
                    s += "$";
                }
            }
            s += ",";
        } else {
            s += "null,";
        }

        if (viajes != null) {
            s += "";
            for (int i = 0; i < this.viajes.size(); i++) {
                s += viajes.get(i).plot();
                if (i < this.viajes.size()) {
                    s += "&";
                }
            }
            s += ",";
        } else {
            s += "null,";
        }

        return s + fotoLicencia + "," + conductor;
    }

    @Override
    public String toString() {
        if (this.conductor) {
//            Chain<Vehiculo> vehiculos, Chain<Ruta> rutas, Chain<Viaje> viajes, String fotoLicencia, boolean conductor) {
            return this.nombreCompleto + "," + this.correoElectronico + " " + this.contrasena + " " + this.fotoPerfil + " " + /*this.vehiculos + 
                    " " + this.rutas + " " + this.viajes + " " +*/ this.fotoLicencia + " " + this.conductor;
        } else {
            return this.nombreCompleto + "," + this.correoElectronico + " " + this.contrasena + " " + this.fotoPerfil;
        }
    }

}
