package Datos;

public class Vehiculo {

    private String placa;
    private String lugar;
    private String color;
    private String modelo;
    private int capacidad;

    public Vehiculo(String placa, String lugar, String color, String modelo, int capacidad) {
        this.placa = placa;
        this.lugar = lugar;
        this.color = color;
        this.modelo = modelo;
        this.capacidad = capacidad;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String plot() {
        return this.getPlaca() + "#" + this.getLugar() + "#" + this.getColor() + "#" + this.getModelo() + "#" + this.getCapacidad();
    }

    @Override
    public String toString() {
        return this.getPlaca() + " , " + this.getLugar() + " , " + this.getColor() + " , " + this.getModelo() + " , " + this.getCapacidad();

    }

}
