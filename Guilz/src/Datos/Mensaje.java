package Datos;
import ListasEncadenadas.*;
import java.util.Date;
public class Mensaje {
    private Pasajero pasajero;
    private String mensaje;
    private Date fecha;

    public Mensaje(Pasajero pasajero, String mensaje, Date fecha) {
        this.pasajero = pasajero;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }

    public Pasajero getPasajero() {
        return pasajero;
    }

    public void setPasajero(Pasajero pasajero) {
        this.pasajero = pasajero;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return pasajero + ": " + mensaje + " || " + fecha;
    }
    
    
}
