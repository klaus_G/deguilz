package Datos;

import ListasEncadenadas.Queue;
import java.util.Date;

public class Viaje {
    private String precio;
    private String horaLlegada;
    private String dias;
    private String cupos;
    private String codRuta;
    private String placavehiculo;
    private Queue<Pasajero> cola;

    public Viaje(String precio, String horaLlegada, String dias, String cupos, String codRuta, String placavehiculo) {
        this.precio = precio;
        this.horaLlegada = horaLlegada;
        this.dias = dias;
        this.cupos = cupos;
        this.codRuta = codRuta;
        this.placavehiculo = placavehiculo;
        this.cola = new Queue<>();
    }

    public Queue<Pasajero> getCola() {
        return cola;
    }

    public void setCola(Queue<Pasajero> cola) {
        this.cola = cola;
    }

    
    
    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public String getCupos() {
        return cupos;
    }

    public void setCupos(String cupos) {
        this.cupos = cupos;
    }

    public String getCodRuta() {
        return codRuta;
    }

    public void setCodRuta(String codRuta) {
        this.codRuta = codRuta;
    }

    public String getPlacavehiculo() {
        return placavehiculo;
    }

    public void setPlacavehiculo(String placavehiculo) {
        this.placavehiculo = placavehiculo;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public String plot() {
        return precio +"="+horaLlegada+"="+dias+"="+cupos+"="+codRuta+"="+placavehiculo;
    }

}
