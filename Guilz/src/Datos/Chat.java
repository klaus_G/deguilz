package Datos;
import ListasEncadenadas.*;
public class Chat {
    private Chain<Mensaje> salaChat;
    private Chain<Pasajero> pasajerosList;

    public Chat(Chain<Mensaje> salaChat, Chain<Pasajero> pasajerosList) {
        this.salaChat = salaChat;
        this.pasajerosList = pasajerosList;
    }

    public Chain<Mensaje> getSalaChat() {
        return salaChat;
    }

    public void setSalaChat(Chain<Mensaje> salaChat) {
        this.salaChat = salaChat;
    }

    public Chain<Pasajero> getPasajerosList() {
        return pasajerosList;
    }

    public void setPasajerosList(Chain<Pasajero> pasajerosList) {
        this.pasajerosList = pasajerosList;
    }   
    
    
}
