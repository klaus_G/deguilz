package Datos;

public class Ruta {
    private String inicio;
    private String recorrido;
    private String llegada;
    private String codigo;
    
    public Ruta(String inicio, String recorrido, String llegada,String codigo) {
        this.inicio = inicio;
        this.recorrido = recorrido;
        this.llegada = llegada;
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    
    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(String recorrido) {
        this.recorrido = recorrido;
    }

    public String getLlegada() {
        return llegada;
    }

    public void setLlegada(String llegada) {
        this.llegada = llegada;
    }
    
    public String plot(){
        return this.inicio + "%" + this.recorrido+ "%" + this.llegada;
    }
    
    @Override
    public String toString(){
        return this.inicio + "," + this.recorrido+ "," + this.llegada;
    }
    
}
