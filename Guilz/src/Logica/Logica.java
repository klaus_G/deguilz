package Logica;

import java.util.Scanner;
import Datos.*;
import static Datos.Pasajero.pasarAlista;
import ListasEncadenadas.Chain;
import ListasEncadenadas.Queue;

public class Logica {

    private Scanner scan;

    public Logica() {
        this.scan = new Scanner(System.in);
    }

    public void pruebaRegistroUsuarios(int n){
        boolean b=false;
        for(int i=0;i<n;i++){
        Generador gen=new Generador();
        Pasajero.registrar(gen.generarUsuario(b));
        if(i==(int)(n/2))b=true;
        }
    }
    
    public void iniciar() {
        System.out.println("||*************GÜILS*************||");
        System.out.println("||      1. Iniciar Sesión        ||");
        System.out.println("||      2. Registrarse           ||");
        System.out.println("||      3. Salir                 ||");
        System.out.println("||*******************************||");

        switch (entradaConsolaEntero()) {
            case 1:
                iniciarSesion();
                break;
            case 2:
                registrarse();
                break;
            case 3:
                System.exit(0);
                break;
            default:
                System.out.println("Entrada invalida.  Vuelva a intentar");
                iniciar();
        }
    }

    public void iniciarSesion() {
        System.out.println("||*********Iniciar Sesion********||");
        System.out.println("|| Digite su correo electrónico: ||");
        String email = scanLinea();
        System.out.println("|| Digite su contraseña:         ||");
        String contrasena = scanLinea();
        if (Pasajero.iniciarSesion(email, contrasena)) {
            Pasajero pasajero = Pasajero.consultar(email);
            menu1(pasajero);
        } else {
            System.out.println("|| Error en las credenciales     ||");
            iniciar();
        }
    }

    public String pedirContrasena() {
        String s = "";
        System.out.println("|| Digite su contraseña:          ||");
        s = scanLinea();
        System.out.println("|| Confirme su contraseña:        ||");
        if (s.equals(scanLinea())) {
            return s;
        } else {
            return pedirContrasena();
        }
    }

    public String pedirFotoPerfil() {
        System.out.println("||¿Desea ingresar foto de perfil?||");
        System.out.println("||(Si/No)                        ||");
        String s = scanLinea();
        if (s.equals("Si") || s.equals("No")) {
            return s;
        } else {
            return pedirFotoPerfil();
        }
    }

    public void registrarse() {
        String nombre = "";
        String correoElectronico = "";
        String contrasena = "";
        String fotoPerfil = "---";

        System.out.println("||************Registro***********||");
        System.out.println("|| Digite su nombre completo:    ||");
        nombre = scanLinea();
        System.out.println("|| Digite su correo electrónico: ||");
        correoElectronico = scanLinea();
        contrasena = pedirContrasena();
        String confirmacionFoto = pedirFotoPerfil();
        if (confirmacionFoto.equals("Si")) {
            System.out.println("|| Digite el nombre del archivo: ||");
            fotoPerfil = scanLinea();
        }

        Pasajero pasajero = new Pasajero(nombre, correoElectronico, contrasena, fotoPerfil);
        Pasajero.registrar(pasajero);
        System.out.println("||**********REGISTRADO***********||");
        System.out.println("||*******************************||");
        iniciar();

    }

    public void editarPerfil(Pasajero pasajero) {
        String dato;
        System.out.println("||**********Editar Perfil********||");
        System.out.println("|| Seleccione el dato a editar   ||\n"
                         + "|| 1. Nombre                     ||\n"
                         + "|| 2. Contraseña                 ||\n"
                         + "|| 3. Foto de pefil              ||\n"
                         + "|| 4. Correo electronico         ||");
        if(pasajero.isConductor()){
            System.out.println("|| 5. Foto licencia              ||");
        }
        System.out.println("|| 6. Volver                     ||\n"
                         + "||*******************************||");   
        dato = scanLinea();
        switch(dato){
            case "1":
                String nombre = "";
                System.out.println("||************Registro***********||");
                System.out.println("|| Digite su nuevo nombre:       ||");
                nombre = scanLinea();
                pasajero.updateNombreCompleto(nombre);
                System.out.println("||*******Nombre actualizado******||");
                editarPerfil(pasajero);
                break;
            case "2":
                String contrasena = "";
                System.out.println("||************Registro***********||");
                System.out.println("|| Digite su nueva  contraseña:  ||");
                contrasena= scanLinea();
                pasajero.updateContrasena(contrasena);
                System.out.println("||*****Contaseña actualizada*****||");
                editarPerfil(pasajero);
                break;
            case "3":
                String fotoPerfil = "";
                System.out.println("||************Registro***********||");
                System.out.println("|| Digite su nueva foto:         ||");
                fotoPerfil = scanLinea();
                pasajero.updateFotoPerfil(fotoPerfil);
                System.out.println("||***Foto de perfil actualizada**||");
                editarPerfil(pasajero);
                break;
            case "4":
                String correo = "";
                System.out.println("||************Registro***********||");
                System.out.println("|| Digite su nuevo correo:       ||");
                correo = scanLinea();
                boolean repetido=false;
                Chain<Pasajero> chain = pasarAlista();
                for (int i = 0; i < chain.size(); i++) {
                    if (chain.get(i).getCorreoElectronico().equals(pasajero.getCorreoElectronico())) {
                        repetido=true;
                    }
                }
                if(repetido){
                    System.out.println("Este  correo electronico ya existe. Intente de nuevo.");
                    editarPerfil(pasajero);
                }else{
                    pasajero.updateCorreoElectronico(correo);
                    System.out.println("||Correo electronico actualizado*||");
                    editarPerfil(pasajero);
                }
                break;
            case "5":
                if(pasajero.isConductor()){
                    String fotoLicencia = "";
                    System.out.println("||************Registro***********||");
                    System.out.println("|| Digite su nueva foto:         ||");
                    fotoLicencia = scanLinea();
                    pasajero.updateCorreoElectronico(fotoLicencia);
                    System.out.println("||*Foto  de licencia actualizada*||");
                    editarPerfil(pasajero);
                }else{
                    System.out.println("Entrada invalida.  Vuelva a intentar");
                    editarPerfil(pasajero);
                }
                break;
            case "6":
                menu1(pasajero);
                break;
            default:
                System.out.println("Entrada invalida. Vuelva a intentar");
                editarPerfil(pasajero);
        }
    }
    
    public String ingresarCodigo(Pasajero pasajero){
        System.out.println("|| Elija la ruta:  ||");
        for(int i=0;i<pasajero.getRutas().size();i++){
        System.out.println(i+" "+pasajero.getRutas().get(i));
        }
        String codigo=scanLinea();
        
        if(Integer.parseInt(codigo)>pasajero.getRutas().size()){
            System.out.println("Este  codigo no existe. Intente de nuevo.");
            ingresarCodigo(pasajero);
        }
        return pasajero.getRutas().get(Integer.parseInt(codigo)).getCodigo();
    }

    public void menuRutas(Pasajero pasajero) {
        String entrada="";
        System.out.println("||*************Rutas*************||");
        System.out.println("|| 1. Mostrar todas las rutas    ||");
        System.out.println("|| 2. Agregar rutas              ||");
        System.out.println("|| 3. Eliminar ruta              ||");
        System.out.println("|| 4. Volver                     ||");
        System.out.println("||*******************************||");
        entrada = scanLinea();
        switch(entrada){
            case "1":
                try{
                    for (int i = 0; i < pasajero.getRutas().size(); i++) {
                        System.out.println(pasajero.getRutas().get(i));
                    }
                }catch(Exception e){
                    System.out.println("No  hay  rutas registradas");
                }
                menuRutas(pasajero);
                break;
            case "2":
                Generador generador = new Generador();
                String inicio="";
                String recorrido="";
                String llegada="";
                String codigo = String.valueOf(generador.randomNum(0, 1000000000))+generador.generarCadenaSinSimbolosLongitudFija(54, 96, 5);
                System.out.println("|| Digite el lugar de inicio:    ||");
                inicio = scanLinea();
                System.out.println("|| Digite el recorrido:          ||");
                recorrido = scanLinea();
                System.out.println("|| Digite el lugar de llegada:   ||");
                llegada = scanLinea();
                pasajero.agregarRuta(new Ruta(inicio, recorrido, llegada, codigo));
                System.out.println("||*********Ruta agregada*********||");
                menuRutas(pasajero);
                break;
            case "3":
                String codigo_ = ingresarCodigo(pasajero);
                Chain<Ruta> chain = pasajero.getRutas();
                for (int i = 0; i < chain.size(); i++){
                    if(chain.get(i).getCodigo().equals(codigo_)){
                        pasajero.eliminarRuta(chain.get(i));
                    }
                }
                break;
            case "4":
                menu1(pasajero);
                break;
            default:
                System.out.println("Entrada invalida. Vuelva a intentar");
                menuRutas(pasajero);
        }
    }
    
    public String ingresarPlaca(Pasajero pasajero, boolean acceso){
        String placa="";
        System.out.println("|| Digite la placa:              ||");
        placa = scanLinea();
        boolean repetido=false;
        Chain<Vehiculo> chain = pasajero.getVehiculos();
        for (int i = 0; i < chain.size(); i++) {
            if (chain.get(i).getPlaca().equals(placa)) {
                repetido=true;
            }
        }
        if(acceso){
            if(repetido){
                System.out.println("Esta placa ya existe. Intente de nuevo.");
                ingresarPlaca(pasajero,true);
            }
            return placa;
        }else{
            if(!repetido){
                System.out.println("Esta placa no existe. Intente de nuevo.");
                ingresarPlaca(pasajero,false);
            }
            return placa;
        }
    }    
    
    public String ingresarCapacidad(){
        String capacidad="";
        try{
            System.out.println("|| Digite La capacidad:          ||");
            capacidad = scanLinea();
            int numerito = Integer.parseInt(capacidad);
        }catch(NumberFormatException e){
            System.out.println("La entrada no es valida. Intente de nuevo.");
            ingresarCapacidad();
        }
        return capacidad;
    }
    
    public void menuVehiculos(Pasajero pasajero){
        String entrada="";
        System.out.println("||************Vehículo***********||");
        System.out.println("|| 1. Mostrar todos los vehiculos||");
        System.out.println("|| 2. Agregar vehiculos          ||");
        System.out.println("|| 3. Eliminar vehiculos         ||");
        System.out.println("|| 4. Volver                     ||");
        System.out.println("||*******************************||");
        entrada = scanLinea();
        switch(entrada){
            case "1":
                try{
                    for (int i = 0; i < pasajero.getVehiculos().size(); i++) {
                        System.out.println(pasajero.getVehiculos().get(i));
                    }
                }catch(Exception e){
                    System.out.println("No hay vehiculos registrados");
                }
                menu1(pasajero);
            case "2":
                String lugar="";
                String placa="";
                String color="";
                String modelo="";
                String capacidad="";
                placa=ingresarPlaca(pasajero,true);
                System.out.println("|| Digite el lugar de expedicion:||");
                lugar = scanLinea();
                System.out.println("|| Digite el color:              ||");
                color = scanLinea();
                System.out.println("|| Digite el modelo:             ||");
                modelo = scanLinea();
                capacidad = ingresarCapacidad();
                pasajero.agregarVehiculo(new Vehiculo(placa, lugar, color, modelo, Integer.parseInt(capacidad)));
                System.out.println("||*******Vehiculo agregado*******||");
                break;
            case "3":
                String placas = ingresarPlaca(pasajero, false);
                Chain<Vehiculo> chain = pasajero.getVehiculos();
                for (int i = 0; i < chain.size(); i++){
                    if(chain.get(i).getPlaca().equals(placas)){
                        pasajero.eliminarVehiculo(chain.get(i));
                    }
                }
                break;
            case "4":
                menu1(pasajero);
                break;
            default:
                System.out.println("Entrada invalida. Vuelva a intentar");
                menuVehiculos(pasajero);
        }
    }
    
    public void menuViajes(Pasajero pasajero){
        System.out.println("||*************Viaje*************||");
        System.out.println("|| 1. Mostrar todos los viajes   ||");
        System.out.println("|| 2. Agregar viaje              ||");
        System.out.println("|| 3. Eliminar viaje             ||");
        System.out.println("|| 4. Volver                     ||");
        System.out.println("||*******************************||");
        switch(entradaConsolaEntero()){
            case 1:
                try{
                    for (int i = 0; i < pasajero.getViajes().size(); i++) {
                        System.out.println(pasajero.getViajes().get(i).toString());
                    }
                }catch(Exception e){
                    System.out.println("No hay viajes registrados");
                }
                menuViajes(pasajero);
            case 2:
                String precio="";
                String hora="";
                String dia="";
                String codigo="";
                String cupos="";
                String placa="";
                System.out.println("|| Digite el precio:             ||");
                precio = scanLinea();
                System.out.println("|| Digite la hora:               ||");
                hora = scanLinea();
                System.out.println("|| Digite los dias:              ||");
                dia = scanLinea();
                System.out.println("|| Digite los cupos:             ||");
                cupos = scanLinea();
                placa = ingresarPlaca(pasajero, false);
                codigo = ingresarCodigo(pasajero);
                
                pasajero.agregarViaje(new Viaje(precio, hora, dia, cupos, codigo, placa));
                System.out.println("||********Viaje agregado*********||");
                menuViajes(pasajero);
                break;
            case 3:
                String codigos = ingresarCodigo(pasajero);
                Chain<Viaje> chain = pasajero.getViajes();
                for (int i = 0; i < chain.size(); i++){
                    if(chain.get(i).getCodRuta().equals(codigos)){
                        pasajero.eliminarViaje(chain.get(i));
                    }
                }
                menuViajes(pasajero);
                break;
            case 4:
                menu1(pasajero);
                break;
            default:
                System.out.println("Entrada invalida. Vuelva a intentar");
                menuVehiculos(pasajero);
        }
    }

    public void menu1(Pasajero pasajero) {
        if (pasajero.isConductor()) {
            System.out.println("||*******************************||");
            System.out.println("||Usuario: " + pasajero.getNombreCompleto());
            System.out.println("||      --CONDUCTOR--            ||");
            System.out.println("||      1. Rutas.                ||");
            System.out.println("||      2. Vehiculos.            ||");
            System.out.println("||      3. Pasajero.             ||");
            System.out.println("||      4. Viajes.               ||");
            System.out.println("||      5. Editar perfil.        ||");
            System.out.println("||      6. Eliminar perfil.      ||");
            System.out.println("||      7. Cerrar sesión.        ||");
            System.out.println("||*******************************||");
            switch (entradaConsolaEntero()) {
                case 1:
                    menuRutas(pasajero);
                    break;
                case 2:
                    menuVehiculos(pasajero);
                    break;
                case 3:
                    
                    break;
                case 4:
                    menuViajes(pasajero);
                case 5:
                    editarPerfil(pasajero);
                    break;
                case 6:
                    pasajero.eliminar();
                    iniciar();
                    break;
                case 7:
                    iniciar();
                    break;
                default:
                    System.out.println("Entrada invalida. Intente de nuevo");
                    menu1(pasajero);
            };
        } else {
            System.out.println("||*******************************||");
            System.out.println("||Usuario: " + pasajero.getNombreCompleto());
            System.out.println("||      --PASAJERO--             ||");
            System.out.println("||      1. Ver rutas.            ||");
            System.out.println("||      2. Convertirse Conductor.||");
            System.out.println("||      3. Editar perfil.        ||");
            System.out.println("||      4. Eliminar perfil.      ||");
            System.out.println("||      5. Pedir un viaje.       ||");
            System.out.println("||      6. Cerrar sesión.        ||");
            System.out.println("||*******************************||");
            switch (entradaConsolaEntero()) {
                case 1:
                    menuRutas(pasajero);
                    break;
                case 2:
                    pasajero.updateConductor(true);
                    menu1(pasajero);
                    break;
                case 3:
                    editarPerfil(pasajero);
                    break;
                case 4:
                    pasajero.eliminar();
                    iniciar();
                    break;
                case  5:
                    System.out.println("|| Seleccione el codigo de ruta: ||");
                    for(int j=0;j<Pasajero.pasarAlista().size();j++){
                        for (int i = 0; i < Pasajero.pasarAlista().get(j).getViajes().size(); i++) {
                            try{
                                System.out.println(Pasajero.pasarAlista().get(j).getViajes().get(i).toString());
                            }catch(Exception e){
                                
                            }
                        }
                    }
                    String cod = scanLinea();
                    boolean counter = true;
                    for(int j=0;j<Pasajero.pasarAlista().size();j++){
                        for (int i = 0; i < Pasajero.pasarAlista().get(j).getViajes().size(); i++) {
                            if(Pasajero.pasarAlista().get(j).getViajes().get(i).getCodRuta().equals(cod)){
                                Queue<Pasajero> cola = Pasajero.pasarAlista().get(j).getViajes().get(i).getCola();
                                cola.enqueue(pasajero);
                                Pasajero.pasarAlista().get(j).getViajes().get(i).setCola(cola);
                                counter = false;
                            }
                        }
                    }
                    if(counter){
                        System.out.println("Entrada invalida. Intente de nuevo");
                        menu1(pasajero);
                    }
                    break;
                case 6:
                    iniciar();
                    break;
                default:
                    System.out.println("Entrada invalida. Intente de nuevo");
                    menu1(pasajero);
            }
        }

    }

    public int entradaConsolaEntero() {
        try {
            Scanner scan = new Scanner(System.in);
            int opcion = scan.nextInt();
            return opcion;
        } catch (Exception e) {
            System.out.println("Entrada inválida. Vuelva a intentar");
            return 0;
        }
    }

    public String scanLinea() {
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }

}
