/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;
import Datos.*;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import ListasEncadenadas.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Flexxo333
 */




public class Generador {
 
    private Chain<Vehiculo> vehiculosDisponibles = new Chain<Vehiculo>();
    private Chain<Pasajero> conductoresDisponibles = new Chain<Pasajero>();
    
    public Generador(){
        
    }
    
    private String generarCadenaSinSimbolosLongitudRandom(int minLength, int maxLength){
        StringBuilder s = new StringBuilder();
        int randomLength = ThreadLocalRandom.current().nextInt(minLength, maxLength+1); //Generar un entero
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 91); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        return new String(s);
    }
    
    private String generarCadenaConSimbolosLongitudRandom(int minLength, int maxLength, int minAscii, int maxAscii){
        StringBuilder s = new StringBuilder();
        int randomLength = ThreadLocalRandom.current().nextInt(minLength, maxLength+1); //Generar un entero
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(minAscii, maxAscii+1); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        
        return new String(s);
    }
    
    public String generarCadenaSinSimbolosLongitudFija(int minLength, int maxLength, int length){
        StringBuilder s = new StringBuilder();
        for(int i=0; i<length; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 91); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        return new String(s);
    }
    
    private String generarCadenaConSimbolosLongitudFija(int minLength, int maxLength, int minAscii, int maxAscii, int length){
        StringBuilder s = new StringBuilder();
        for(int i=0; i<length; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(minAscii, maxAscii+1); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        
        return new String(s);
    }
    
    public int randomNum(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max+1);
    }
    
    //Para vehiculos
    private String generarPlaca(){
        String placa = "";
        for(int i=0; i<3; i++){
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 90 + 1); //Generar un entero en [65,91]
            placa += Character.toString((char) randomLetter); //Convertir el entero a su representación ASCII y concatenar
        }        
        int randomNum = ThreadLocalRandom.current().nextInt(0, 1000); //Parte numérica de la placa
        placa += randomNum;
        return placa;
    }
    
    private String generarLugar(){
        String[] ciudades ={ "Bogotá", "Medellín", "Cali", "Bucaramanga", "Cartagena", "Tunja", "La Dorada", "Fusagasuga", "Ibagué"};
        String ciudad = "";
        int index = ThreadLocalRandom.current().nextInt(0, ciudades.length);
        ciudad += ciudades[index];
        return ciudad;
    }
    
    private String generarColor(){
        String[] colores ={ "Rojo", "Azul", "Amarillo", "Verde", "Gris", "Negro", "Purpura", "Naranja", "Marrón"};
        String color = "";
        int index = ThreadLocalRandom.current().nextInt(0, colores.length);
        color += colores[index];
        return color;
    }
    
    private String generarModelo(){
        String[] marcas ={ "Hyundai", "Mazda", "Subaru", "Renault", "Chevrolet" , "Lamborghini", "Bugatti", "Ferrari", "Pollito" };
        String marca = "";
        int index = ThreadLocalRandom.current().nextInt(0, marcas.length);
        marca += marcas[index];
        return marca;
    }
    
    private int generarCapacidad(){
        return ThreadLocalRandom.current().nextInt(3, 5);
    }
    
    public Vehiculo generarVehiculo(){
        Vehiculo v = new Vehiculo(generarPlaca(), generarLugar(), generarColor(), generarModelo(), generarCapacidad() );
        return v;
    }
    
    //Generar Usuario
    
    private String generarNombreCompleto(){
        String[] nombres ={ "Cristian", "Daniela", "Laura", "Paula", "Monica", "Jose", "Ivan", "David", "Sebastian", "Sofia", "Juana", "Daniel", "Rodrigo", "Santiago" , "Lorena"};
        String[] apellidos ={ "Tovar", "Bejarano", "Solano", "Martinez", "Cornejo", "Cardenas", "Vallejo", "Ruiz", "Espinoza", "Valencia", "Santana"};
        StringBuilder s = new StringBuilder();
        int indexNom = ThreadLocalRandom.current().nextInt(0, nombres.length);
        int indexApe = ThreadLocalRandom.current().nextInt(0, apellidos.length);
        
        s.append(nombres[indexNom]);
        s.append(" ");
        s.append(apellidos[indexApe]);
        return new String(s);
    }
    
    private String generarCorreoElectrónico(){
        
        //Determinar longitud pseudoaleatoria del email
        int randomLength = ThreadLocalRandom.current().nextInt(8, 20); //Generar un entero [8,20)
        StringBuilder s = new StringBuilder();
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 90 + 1); //Generar un entero en [65,91)
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        s.append("@");
        String[] dominios = {"hotmail.com", "hotmail.es", "gmail.com", "outlook.com", "outlook.es", "yahoo.es"};
        int indexDominio = ThreadLocalRandom.current().nextInt(0, dominios.length);
        s.append(dominios[indexDominio]);
        return new String(s);
    }
    
    private String generarContrasena(){
        //Determinar longitud pseudoaleatoria del email
        int randomLength = ThreadLocalRandom.current().nextInt(8, 20); //Generar un entero
        StringBuilder s = new StringBuilder();
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(33, 96); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        return new String(s);
    }
    
    private String generarFotoDePerfil(){
        String fileName = generarCadenaConSimbolosLongitudRandom(5, 10, 65, 91);
        String[] formats = {".jpg", ".png", ".gif"};
        int index = randomNum(0, formats.length-1);
        return fileName += formats[index];
    }
    
    public Chain<Vehiculo> generarListaDeVehiculos(Pasajero conductor, int maximo){
        if (conductor.isConductor()) {
            Chain<Vehiculo> vehiculos = new Chain<Vehiculo>(); //crear la estructura
            int cuantos = randomNum(1, maximo+1);//Cuantos vehiculos? minimo 1        
            for (int i = 0; i < cuantos; i++) {
                vehiculos.add( generarVehiculo() );
            }
            conductor.setVehiculos(vehiculos);
            return vehiculos;
        }  else {
            return null;
        }
    }
    
    public Ruta generarRuta(){
        //Ruta(String inicio, String recorrido, String llegada)
        String[] origenes = {"Maiporen", "Soacha Compartir", "Ducales" , "Perdomo", "Timiza", "Guacamayas", "Chuniza", "Modelia", "Isla del Sol", "San Francisco", "Candelaria", "Roma", "Aures"};
        int index = randomNum(0, origenes.length-1);
        String index_ = String.valueOf(randomNum(0, 1000000000))+generarCadenaSinSimbolosLongitudFija(54, 96, 5);
        Ruta ruta = new Ruta(origenes[index], " recorrido ", "Universidad Nacional de Colombia",index_);
        return ruta;
    }
    
    public Chain<Ruta> generarListaRutas(Pasajero conductor, int maximo){
        if (conductor.isConductor()) {
            Chain<Ruta> rutas = new Chain<Ruta>();
            int cuantas = randomNum(1, maximo+1);
            for(int i=0; i<cuantas; i++){
                rutas.add( generarRuta() );
            }
            conductor.setRutas(rutas);
            return rutas;
        } else {
            return null;
        }
    }
    
    public Viaje generarViaje(Pasajero conductor){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        StringBuilder d = new StringBuilder();
        d.append(randomNum(1, 31)).append("-");
        d.append(randomNum(1, 13)).append("-");
        d.append(randomNum(2019, 2021)).append(" ");
        d.append(randomNum(0, 23)).append(":"); //hora
        d.append(randomNum(0, 60)).append(":"); //minuto
        d.append(randomNum(0, 60)); //seg
        
	String dateInString = new String(d);
        Date date = new Date(); //fecha del viaje
        
        try {
            date = sdf.parse(dateInString);
        } catch (ParseException ex) {
            Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
        }
        StringBuilder d1 = new StringBuilder();
        SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
        d1.append(randomNum(0, 23)).append(":"); //hora
        d1.append(randomNum(0, 60)).append(":"); //minuto
        d1.append(randomNum(0, 60)); //seg
        Date horaLlegada = new Date();
        String hora = new String(d1);
        try {
            horaLlegada = sdf2.parse(hora);
        } catch (ParseException ex) {
            Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Elegir un conductor
        Vehiculo vehiculoDelViaje = conductor.getVehiculos().get( randomNum(0, conductor.getVehiculos().size()-1 ));
        Viaje viaje = new Viaje( String.valueOf(randomNum(4000, 6000)), horaLlegada.toString(), date.toString(),String.valueOf(randomNum(1, 4)),  vehiculoDelViaje.getPlaca(), conductor.getRutas().get( randomNum(0, conductor.getRutas().size()-1 ) ).getCodigo());       
        return viaje;
    }
    
    public Chain<Viaje> generarListaDeViajes(Pasajero conductor, int maximo){
        if (conductor.isConductor()) {
            Chain<Viaje> listaViajes = new Chain<Viaje>();
            int cuantos = randomNum(1, maximo+1);
            for (int i=0; i<cuantos; i++){
                listaViajes.add( generarViaje(conductor) );
            }
            conductor.setViajes(listaViajes);
            return listaViajes;
        } else {
            return null;
        }
    }
    
    //public Viaje(int precio, Date horaLlegada, Date dias, Vehiculo vehiculo, Ruta ruta) {
    
    private String generarFotoLicencia(){
        String fileName = generarCadenaSinSimbolosLongitudRandom(5, 10);
        String[] formats = {".jpg", ".png", ".gif"};
        int index = randomNum(0, formats.length-1);
        return fileName += formats[index];
    }
    
    private boolean generarEstado(){
        int num = randomNum(0, 2);
        if(num>0){
            return true;
        } else {
            return false;
        }
    }
    
    public Pasajero generarUsuario(boolean conduce){
        Pasajero pasajero;
        //boolean conduce = generarEstado();        Habilitar la linea para que el usuario sea conductor o pasajero de manera pseudoaleatoria
        if(!conduce){
            pasajero = new Pasajero(generarNombreCompleto(), generarCorreoElectrónico(), generarContrasena(), generarFotoDePerfil());
        } else {
            Chain<Vehiculo> listaVehiculos = new Chain<Vehiculo>();
            Chain<Ruta> listaRutas = new Chain<Ruta>();
            Chain<Viaje> listaViajes = new Chain<Viaje>();
            pasajero = new Pasajero(generarNombreCompleto(), generarCorreoElectrónico(), generarContrasena(), generarFotoDePerfil(), 
                    listaVehiculos,  listaRutas, listaViajes, generarFotoLicencia(), true); //Generar el usuario, sin vehiculos, ni rutas, ni viajes.
            conductoresDisponibles.add(pasajero);
            
        }
        return pasajero;
    }
    
    //
}
