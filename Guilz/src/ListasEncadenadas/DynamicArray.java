/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasEncadenadas;

/**
 *
 * @author Flexxo333
 */
public class DynamicArray<T> {
    
    private int capacity;
    private int size;
    private transient T[] data;

    public DynamicArray() {
        this.size = 0;
        this.capacity = 1;
        this.data= (T[]) new Object[capacity];
    }
    
    
    
    public void checkIndex(int index) {
        if(index <0 || index >= data.length){
            throw new IndexOutOfBoundsException("index = " + index + " capacity = " + data.length);
        }
    }
    
    public T get(int index){
        checkIndex(index);
        return data[index];        
    }
    
    public void set(int index, T value){
        checkIndex(index);
        data[index] = value;
    }
    
    public void pushBackValue(T value){
        if(this.size == capacity){
            T[] newArray = (T[]) new Object[ 2 * capacity];
            for(int i=0; i<data.length; i++){
                newArray[i] = data[i];
            }
            this.data = newArray;
            this.capacity = capacity *2;
        }
        
        this.data[size] = value;
        size += 1;
    }
    
    public void remove(int index){
        checkIndex(index);
        for(int j=index; j<data.length-2; j++){
            this.data[j] = this.data[j+1];
        }
        size -= 1;
    }
    
    public int size(){
        return this.size;
    }
    
    public int capacity(){
        return this.capacity;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<this.size ; i++){
            sb.append(this.data[i]).append(" ");
        }
        return new String(sb);
    }
    
}
