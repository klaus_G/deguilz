/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasEncadenadas;

public class Stack<T> implements StackInterface<T> {

    private Chain<T> stack;

    public Stack() {
        this.stack = new Chain<>();
    }

    @Override
    public T pop() {
        T ret = stack.get(stack.size - 1);
        stack.remove(stack.size - 1);
        return ret;
    }

    @Override
    public T peek() {
        return stack.get(stack.size - 1);
    }

    @Override
    public void add(T element) {
        stack.add(stack.size, element);
    }

    @Override
    public boolean isEmpty() {
        return stack.size == 0;
    }

    @Override
    public String toString() {
        String s="[";
        
        for(int i=0;i<stack.size;i++){
            s+=(i!=stack.size-1)?stack.get(i)+" ":stack.get(i);
        }
        return s+"]\n";
    }

    
}

