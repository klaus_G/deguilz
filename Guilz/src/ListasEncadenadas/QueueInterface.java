/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasEncadenadas;

public interface QueueInterface<T> {

    public T dequeue();

    public void enqueue(T element);

    public T peek();

    public boolean isEmpty();
}
