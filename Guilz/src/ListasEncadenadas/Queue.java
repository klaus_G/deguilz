/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasEncadenadas;

public class Queue<T> implements QueueInterface<T>{

    Chain<T> chainList = new Chain<>();

    @Override
    public T dequeue() {
        T valor = chainList.get(0);
        chainList.remove(0);
        return valor;
    }

    @Override
    public void enqueue(T element) {
        chainList.add(chainList.size, element);
    }

    @Override
    public T peek() {
        return chainList.get(0);
    }

    @Override
    public boolean isEmpty() {
        return chainList.size == 0;
    }

}
