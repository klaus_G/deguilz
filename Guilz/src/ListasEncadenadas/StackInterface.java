/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasEncadenadas;

public interface StackInterface<T>{
    public T pop();
    
    public T peek();
    
    public void add(T element);
    
    public boolean isEmpty();
}
